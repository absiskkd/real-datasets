#Used to sort the original files according to time
#Dont forget to change path of the files if you want to use. 


import csv

inputFile = open("/home/agetache/DYNAMICDATASETS/music/originalfile/user_taggedartists-timestamps.dat","r")
lines = inputFile.read().splitlines() # this removes the '\n' from the lines read from the file

def takeThirdColumn(line):                      # taking the third column of every row passed for sorting
        return eval(line.split()[3].strip())


#print(lines)
#print("\n --- SORTED --- \n")
sortedRows = sorted(lines,key=takeThirdColumn) # this built in sort function sorts the list using the third column since we specified it as a key
#print(sortedRows)

inputFile.close()
sortedInputFile = open("/home/agetache/DYNAMICDATASETS/music/sortedfiles/sorted.txt", "w")
for sortedRow in sortedRows:
        sortedInputFile.write("%s\n" %sortedRow)

#newFile = open("outputFile.txt", "w")
#newFile.write("Numbers from column 3 below 300 are:\n")

#for line in sortedRows:
#       thirdColumn = line.split(' ')[2].strip()
#
#       if (eval(thirdColumn) < 300):
#               newFile.write("%s\n" %line.strip())

#newFile.close()

