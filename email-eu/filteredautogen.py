inputFile = open("/home/agetache/DYNAMICDATASETS/email-eu/sortedFiles/sortedDept4.txt","r")
lines = inputFile.read().splitlines() 

fileNumber = 0
timeFactorMin = 0
timeFactorMax = timeFactorMin + 500000

#toDiscardFile = open("/home/agetache/DYNAMICDATASETS/email-eu/filteredfiles/discardedfiles/newDiscardedValuestoDiscardDept1File.txt", "w") 
toDiscard = []

def doTask():
	newFile = open("/home/agetache/DYNAMICDATASETS/email-eu/filteredfiles/filesDept4/File" + str(fileNumber) + ".txt", "w")

	count = 0
	toWrite = []
	allValuesInRange = [x for x in lines if eval(x.split(' ')[2]) < timeFactorMax]

	for line in lines:
		thirdColumn = line.split(' ')[2].strip()

		# if (eval(thirdColumn) <= timeFactorMin):
				#newFile.write("%s\n" %line.strip())
			# continue
		if (eval(thirdColumn) >= timeFactorMax):
			break
		else:
			# print(allValuesInRange)
			allPossibleValues = [x for x in allValuesInRange if x.split(' ')[0] == line.split(' ')[0] and x.split(' ')[1] == line.split(' ')[1]]

			if len(allPossibleValues) > 1:
				lastElement = allPossibleValues[len(allPossibleValues) - 1]
				if lastElement not in toWrite:				
					toWrite.append(lastElement)

				for discard in allPossibleValues:
					if discard != lastElement and discard not in toDiscard:
						toDiscard.append(discard)

			else:
				toWrite.append(line)
			count += 1
	# print(toWrite)
	# print(toDiscard)
	for currentLine in toWrite:
		newFile.write("%s\n" %currentLine.strip())

	print(count)
	newFile.close()



if __name__  == "__main__":
	totalNumberOfFiles = input("Enter the total number of files: ")

	while(fileNumber <= totalNumberOfFiles):
		doTask()
		fileNumber += 1
		timeFactorMin = timeFactorMax
		timeFactorMax = timeFactorMin + 500000

	inputFile.close()

	for currentDiscardedLine in toDiscard:
		toDiscardFile.write("%s\n" %currentDiscardedLine.strip())
	toDiscardFile.close()

