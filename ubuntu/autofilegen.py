#A simple python script to automatically generate files given a certain range of time. Range of time in this case is 1,000,000s.
#Files must be sorted first to get the accurate out put we want. 
#The amount of files we want is determined on the counsle by the user. We have created 177 files from this specific datasets. 
#Remeber to change the range of time when you consider changing the number of files you want for more ideal outputs of your datasets.


inputFile = open("/home/agetache/DYNAMICDATASETS/ubuntu/sortedFiles/sec-askubuntu-c2asorted.txt","r")
lines = inputFile.read().splitlines() 

fileNumber = 0
timeFactorMin = 0
timeFactorMax = timeFactorMin + 1000000

def doTask():
	newFile = open("/home/agetache/DYNAMICDATASETS/ubuntu/c2afiles/File" + str(fileNumber) + ".txt", "w")
	#newFile.write("Numbers from column 3 below 300 are:\n")
	count = 0
	for line in lines:
		thirdColumn = line.split(' ')[2].strip()

#		if (eval(thirdColumn) <= timeFactorMin):
				#newFile.write("%s\n" %line.strip())
#			continue
		if (eval(thirdColumn) > timeFactorMax):
				#newFile.write("%s\n" %line.strip())
			break
		else:
			newFile.write("%s\n" %line.strip())
			count += 1
	print(count)
	# print("Time Factor Min == " + str(timeFactorMin) + " " + "Time Factor Max == " + str(timeFactorMax))

	newFile.close()


if __name__  == "__main__":
	totalNumberOfFiles = input("Enter the total number of files: ")

	while(fileNumber <= totalNumberOfFiles):
		doTask()
		fileNumber += 1
		timeFactorMin = timeFactorMax
		timeFactorMax = timeFactorMin + 1000000

	inputFile.close()
