#Used to remove the third column from the files we generated
#Don't forget to chnage the path of your files when using. 

fileNumber = 1

def removeThirdColumn():
	inputFile = open("/home/agetache/temporal/ubuntu/generalfiles/File" + str(fileNumber) + ".txt", "r+")
	lines = inputFile.read().splitlines()

	inputFile.seek(0) # moves to beginning of file cause the current position is end of file
	inputFile.truncate() # resizes / clears the file

	for line in lines:
		thirdColumn = line.split(' ')[2].strip()
		removedLine = line.replace(thirdColumn, '')
		inputFile.write("%s\n" %removedLine.strip())

	inputFile.close()

if __name__ == '__main__':
	numberOfFiles = input("Enter number of files: ")
	while (fileNumber <= numberOfFiles):
		removeThirdColumn()
		fileNumber += 1
	print("Done!")
