import csv
import sys
import argparse
import random
          
flagParser = argparse.ArgumentParser(description='[description]')
flagParser.add_argument('-i', '--initialFile', default='File1.txt', type=str, help="Input File (original elist)")
flagParser.add_argument('-f', '--finalFile', default='File2.txt', type=str, help="Input File (next elist)")
flagParser.add_argument('-o', '--outputFile', default='clog2.txt', type=str, help="Output File (clog)")

args = flagParser.parse_args()
iF = args.initialFile 
fF = args.finalFile
outF = args.outputFile
    
print "Input File 1: %s" % iF
print "Input File 2: %s" % fF
print "Output File: %s" % outF

# print "Output File: %s" % outF
# print "Output File: %s" % outF

### Read initial file ###
with open(iF, "r") as f1:
        linesInitial = f1.readlines()
        linesInitial = [x.strip().split() for x in linesInitial]

initialEdges = []

### Read final file ###

with open(fF, "r") as f2:
        linesInitialFinal = f2.readlines()
        linesInitialFinal = [x.strip().split() for x in linesInitialFinal]

# print len(initialEdges)
# print linesInitial
# for line in linesInitial:
#         # print "line[0]:",line[0],"line[1]:",line[1]
#         if len(initialEdges) <  line[0]:
#                 initialEdges.append([])
#         initialEdges[int(line[0])].append(int(line[1]))
#                 initialEdges.append([])
#         initialEdges[int(line[0])].append(int(line[1]))


### Make the initial file's list and final file's list equal by adding empty lists in the inital list until it's equal to the length of the final file's list
index = 0
# print len(linesInitialFinal)
while index < len(linesInitialFinal):
        if len(linesInitial) <= index:
                initialEdges.append([])
        else:
                initialEdges.append(linesInitial[index])
        index += 1


# print len(initialEdges)



finalEdges = linesInitialFinal
# for line in linesInitial:
#         #print "line[0]:",line[0],"line[1]:",line[1]
#         if len(finalEdges) < linei[0]:
#                 finalEdges.append([])
#         finalEdges[int(line[0])].append(int(line[1]))

with open(outF, "w") as f:
        ### Check for edges to remove ###
        vertex = 0
        for neighbors in initialEdges:
                for neighbor in neighbors:
                        # print(finalEdges[vertex])
                        if neighbor not in finalEdges[vertex]:
                                f.write("E\t-\t%s" % str(vertex))
                                f.write("\t%s\n" % str(neighbor))
                vertex = vertex + 1

        ### Check for edges to add ###
        # print(initialEdges)
        # print(initialEdges.index(finalEdges[len(initialEdges)]))
        vertex = 0
        for neighbors in finalEdges:
                for neighbor in neighbors:
                        if neighbor not in initialEdges[vertex]:
                                f.write("E\t+\t%s" % str(vertex))
                                f.write("\t%s\n" % str(neighbor))
                vertex = vertex + 1

#vertex = 1
#with open(outF, "w") as f:
#       for line in linesInitial:
#               for neighbor in line:
#                       f.write("%s\t" % vertex)
#                       f.write("%s\n" % neighbor)
#               vertex += 1

# print linesInitial
#numVertices = len(vertices)
#current_partition = 1
#assignment = {}

#for y in range(numVertices):
#       selected_vertex = random.sample(vertices, 1)
#       selected_vertex = selected_vertex[0]
#       assignment[selected_vertex] = current_partition
#       vertices.remove(selected_vertex)
#       current_partition += 1
#
#       if (current_partition > numP):
#               current_partition = 1
#
#with open(outF, "w") as f:
#       for key in assignment:
#               f.write("%s\t" % str(key))
#               f.write("%s\n" % str(assignment[key]))


